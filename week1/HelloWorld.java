package week1;

public class HelloWorld {
	public static void main (String args[]) {
		Circle circle = new Circle();
		circle.setX(-2.06 );
		circle.setY(-0.23);
		circle.setR(1.33);
		// state1
		circle.setX(0);
		circle.setY(5);
		circle.setR(1.88);
		// state2
		circle.translate(5.0, 0.0);
		// state3
		circle.scale(0.5);
		// state 4 
		circle.translate(-1.0, -1.0);
		circle.setR(3.1);
		// state5
		
	}

}
