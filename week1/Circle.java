package week1;
import java.math.*;
public class Circle {
	
	public double x ;
	public double y;
	public double radius;
	
	public double getX() {
		return x;
	}
	public void setX(double newX) {
		x = newX ;
	}
	public double getY() {
		return y;
	}
	public void setY(double newY) {
		y = newY ;
	}
	public double getR() {
		return radius;
	}public void setR(double newR) {
		radius = newR ;
	}
	public double getArea(){
		return (Math.PI * radius * radius);	
		}
	public void translate (double dx , double dy) {
		x += dx ;
		y += dy ;
	}
	public double scale (double scaler) {
		return radius = radius * scaler ;
	}
	

	 
	

}
